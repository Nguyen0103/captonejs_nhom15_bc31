const BASE_URL = "https://62b0787ee460b79df0469c94.mockapi.io/Products";

export let productService = {
    getProductList: function () {
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    }
}