import { productController } from "./controller/productController.js";
import { productService } from "./service/productService.js";

let productList = [];
let cloneCart = [];

//click cover will be turn off cover
productController.sideSwitch();
// Show/Hide side Nav
let sideNav = (num) => {
    productController.cartSwitch(num);
};
window.sideNav = sideNav;

// ---------------------------------------------------------------------------------------------

let dataJson = localStorage.getItem("productListJson");
if (dataJson !== null) {
    let productArr = JSON.parse(dataJson);
    cloneCart = [...productArr];
}
productController.renderCart(cloneCart);
productController.totalQty(cloneCart);

let renderProductService = () => {
    productService
        .getProductList()
        .then((res) => {
            productList = res.data;
            productController.renderProductList(productList);
        })
        .catch((err) => { });
};
renderProductService();

let selected = () => {
    let value = document.getElementById("selected").value;

    if (value === "0") {
        renderProductService();
    }

    let cart = productList.filter((item) => {
        return item.type === value;
    });
    productController.renderProductList(cart);
};
window.selected = selected;

let addItem = (id) => {
    productService
        .getProductList()
        .then((res) => {
            productList = res.data;
            let cart = productController.addProductToCart(id, productList, cloneCart);
            productController.renderCart(cart);
            productController.totalQty(cart);

            var productListJson = JSON.stringify(cart);
            localStorage.setItem("productListJson", productListJson);
        })
        .catch((err) => { });
};
window.addItem = addItem;

let deleteCartItem = (id) => {
    let cart = [...cloneCart];
    let index = cart.findIndex((item) => {
        return item.id == id;
    });
    cart.splice(index, 1);
    productController.renderCart(cart);
    cloneCart = [...cart];
    productController.totalQty(cloneCart);

    var productListJson = JSON.stringify(cart);
    localStorage.setItem("productListJson", productListJson);
};
window.deleteCartItem = deleteCartItem;

let changeQuantity = (id, step) => {
    let cart = [...cloneCart];
    let index = cart.findIndex((item) => {
        return item.id == id;
    });
    cart[index].quantity += step;

    if (cart[index].quantity === 0) {
        cart.splice(index, 1);
    } // when the shopping cart only have item quantity is 1 will be deleted
    productController.renderCart(cart);
    productController.totalQty(cart);
    cloneCart = [...cart];

    var productListJson = JSON.stringify(cart);
    localStorage.setItem("productListJson", productListJson);
};
window.changeQuantity = changeQuantity;

let clearAllCart = () => {
    let cart = [...cloneCart];
    cart = [];

    productController.renderCart(cart);
    productController.totalQty(cart);
    cloneCart = [...cart];

    var productListJson = JSON.stringify(cart);
    localStorage.setItem("productListJson", productListJson);
};
window.clearAllCart = clearAllCart;

let buy = (index) => {
    switch (index) {
        case 1: {
            if (cloneCart.length > 0) {
                document.getElementById("invoice").style.display = "block";
                document.getElementById("cart").style.display = "none";
                let cart = [...cloneCart];
                productController.renderInvoice(cart);
            }
            break;
        }
        case 0:
            {
                document.getElementById("invoice").style.display = "none";
                document.getElementById("cart").style.display = "block";
            }
            break;
    }
};
window.buy = buy;

let order = () => {
    cloneCart = [];

    productController.renderCart(cloneCart);
    productController.totalQty(cart);

    var productListJson = JSON.stringify(cloneCart);
    localStorage.setItem("productListJson", productListJson);

    buyer(0);
};
window.order = order;
